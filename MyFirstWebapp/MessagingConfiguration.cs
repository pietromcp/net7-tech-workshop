namespace MyFirstWebapp; 

public interface MessagingConfiguration {
    string Text { get; }
}

public class SystemConfigurationAwareMessagingConfiguration : MessagingConfiguration {
    private readonly IConfiguration cfg;

    public SystemConfigurationAwareMessagingConfiguration(IConfiguration cfg) {
        this.cfg = cfg;
    }

    public string Text => cfg.GetSection("messaging")
        .GetSection("text")
        .Value;
}
