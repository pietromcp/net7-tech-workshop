using Microsoft.AspNetCore.Mvc;

namespace MyFirstWebapp; 

[ApiController]
[Route("/api/messages")]
public class MessageController : ControllerBase {
    private readonly MessageProvider msgProvider;

    public MessageController(MessageProvider msgProvider) {
        this.msgProvider = msgProvider;
    }

    [HttpGet]
    public MessageResponse GetMessage() => new MessageResponse(msgProvider.ProvideMessage());

    [HttpGet]
    [Route("not-found")]
    public IActionResult ReturnNotFound() => NotFound();
    
    [HttpGet]
    [Route("not-found-2")]
    public IActionResult ReturnNotFound2() => NotFound("Impossibile trovare la risorsa richiesta");
}

public record MessageResponse(string Text);
