using MyFirstWebapp;

var builder = WebApplication.CreateBuilder(args);


var configuredMessage = builder.Configuration.GetSection("message").Value ?? "Default message";
// var messagingConfiguration = new MessagingConfiguration();
// builder.Configuration.GetSection("messaging")
//     .Bind(messagingConfiguration);
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton<MessagingConfiguration, SystemConfigurationAwareMessagingConfiguration>();
builder.Services.AddSingleton<MessageProvider, FixedMessageProvider>();
// builder.Services.AddSingleton<MessageProvider>(p => new FixedMessageProvider(configuredMessage));
//builder.Services.AddSingleton<MessageProvider, RandomMessageProvider>();
builder.Services.AddSingleton<RandomGenerator, SystemRandomGenerator>();
builder.Services.AddSingleton<VeryPoorRandomGenerator>();
// builder.Services.AddSingleton<RandomGenerator, VeryPoorRandomGenerator>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment()) {
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
