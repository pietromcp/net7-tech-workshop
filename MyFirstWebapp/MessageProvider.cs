namespace MyFirstWebapp; 

public interface MessageProvider {
    string ProvideMessage();
}

public class FixedMessageProvider : MessageProvider {
    private readonly MessagingConfiguration cfg;

    public FixedMessageProvider(MessagingConfiguration cfg) {
        this.cfg = cfg;
    }
    // public FixedMessageProvider(string message) {
    //     this.message = message;
    // }

    public string ProvideMessage() {
        return cfg.Text;
    }
}

public class HardcodedMessageProvider : MessageProvider {
    public string ProvideMessage() {
        return "Hello, World!!";
    }
}

public class RandomMessageProvider : MessageProvider {
    private static readonly IList<string> Messages = new[] {
        "Hello, World!",
        "Salve, gente!",
        "Hello World senza virgola!",
        "¡Hola, mundo!",
        "Hi"
    };

    private readonly RandomGenerator rnd;

    public RandomMessageProvider(VeryPoorRandomGenerator rnd) {
        this.rnd = rnd;
    }

    public string ProvideMessage() {
        return Messages[rnd.NextInt(Messages.Count)];
    }
}

public interface RandomGenerator {
    int NextInt(int max);
}

public class SystemRandomGenerator : RandomGenerator {
    private readonly Random rnd = new Random();
    public int NextInt(int max) {
        return rnd.Next(max);
    }
}

public class VeryPoorRandomGenerator : RandomGenerator {
    public int NextInt(int max) {
        return DateTime.Now.Second % max;
    }
}