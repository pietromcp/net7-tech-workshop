using System.Collections;
using Microsoft.AspNetCore.Mvc;

namespace MyFirstWebapp.Controllers;

[ApiController]
[Route("weather-forecast")]
// [Route("[controller]")]
public class WeatherForecastController : ControllerBase {
    private static readonly string[] Summaries = new[] {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly ILogger<WeatherForecastController> _logger;

    public WeatherForecastController(ILogger<WeatherForecastController> logger) {
        _logger = logger;
    }

    [HttpGet(Name = "GetWeatherForecast")]
    // [Route("")]
    public IEnumerable<WeatherForecast> Get(int? mintemp) {
        return Enumerable.Range(1, 5)
            .Select(index => new WeatherForecast {
                Date = DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
                TemperatureC = Random.Shared.Next(-20, 55),
                Summary = Summaries[Random.Shared.Next(Summaries.Length)]
            })
            .Where(x => !mintemp.HasValue || x.TemperatureC >= mintemp.Value)
            .ToArray();
    }

    [HttpGet]
    [Route("log")]
    public RequestLog Log([FromQuery] int x, [FromQuery] string y, [FromQuery] int[] z) {
        return new RequestLog(x, y, z);
    }
    
    [HttpGet]
    [Route("log2")]
    public RequestLog Log2([FromQuery] RequestLogInput2 input) {
        var list = input.W != null ? input.Z.Concat(new[] { input.W.A }) : input.Z;
        return new RequestLog(input.X, input.Y, list.ToArray());
    }
    
    [HttpGet]
    [Route("void")]
    public void Void() {
        
    }
    
    [HttpPost]
    [Route("log")]
    public RequestLog PostRquestForLog([FromBody] RequestLogInput input) {
        return new RequestLog(input.X, input.Y, input.Z);
    }
    [HttpGet]
    [Route("{day}")]
    public IActionResult Get(string day) {
        var d = DateOnly.ParseExact(day, "yyyy-MM-dd");
        var min = DateOnly.FromDateTime(DateTime.Now.AddDays(1));
        var max = DateOnly.FromDateTime(DateTime.Now.AddDays(5));
        if (d < min || d > max) {
            return NotFound();
        }
        return Ok(new WeatherForecast {
            Date = d,
            TemperatureC = Random.Shared.Next(10, 55),
            Summary = Summaries[Random.Shared.Next(Summaries.Length)]
        });
    }

    [HttpGet]
    [Route("teapot")]
    public IActionResult Teapot() {
        return new StatusCodeResult(418);
    }
    
    [HttpGet]
    [Route("created")]
    public IActionResult Created() {
        Response.Headers.ETag = "123";
        Response.Headers.Add("X-Day", "2023-03-19");
        return Created("/weather-forecast/2023-03-19", null);
    }
    
    [HttpGet]
    [Route("file")]
    public IActionResult File() {
        return File(System.IO.File.OpenRead("/home/pietro/Desktop/Manning.Functional.Programming.in.Scala.2014.8.pdf"), "applicatiojn/pdf", "FPIS.pdf");
    }

    [HttpGet]
    [Route("{day}/sum/{summary}")]
    public WeatherForecast Get(string day, string summary) {
        return new WeatherForecast {
            Date = DateOnly.ParseExact(day, "yyyy-MM-dd"),
            TemperatureC = Random.Shared.Next(10, 55),
            Summary = summary
        };
    }
}

public record RequestLog(int A, string B, int[] C);
public record RequestLogInput(int X, string Y, int[] Z);
public record RequestLogInput2(int X, string Y, int[] Z, RequestLogNestedInput W);
public record RequestLogNestedInput(int A);